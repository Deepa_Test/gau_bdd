package stepdefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class UserValidStepDef {

        @Given("^I have some step definitions$")
        public void i_have_some_step_definitions() throws Throwable {
            // Write code here that turns the phrase above into concrete actions

        }
        @When("^I write a Hello World Cucumber Test$")
        public void I_write_a_Hello_World_Cucumber_Test() throws Throwable {
            // Write code here that turns the phrase above into concrete actions

        }
        @Then("^I should see \"([^\"]*)\" printed on the screen$")
        public void I_should_see_printed_on_the_screen(String value) throws Throwable {
            if(value.equalsIgnoreCase("Hello World")){
                assertTrue(true);
            }
            else{
                assertFalse(true);
            }

        }

}
