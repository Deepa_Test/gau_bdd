@SmokeModule
Feature: BDD implementation using Cucumber

  Scenario Outline: Launching Cucumber
    Given I have some step definitions
    When I write a Hello World Cucumber Test
    Then I should see "<value>" printed on the screen

       Examples:
      |value|
      |Hello World|
